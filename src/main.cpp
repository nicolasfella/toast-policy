#include <QtGlobal>

#ifndef Q_OS_ANDROID
#include <QApplication>
#else
#include <QGuiApplication>
#endif

#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QSettings>

class Settings : public QObject
{
    Q_OBJECT
public:
    Q_INVOKABLE void setValue(const QString &type, int index, bool checked)
    {
        m_settings.beginGroup(type);
        m_settings.setValue(QString::number(index), checked);
        m_settings.endGroup();
    }

    Q_INVOKABLE bool readValue(const QString &type, int index)
    {
        m_settings.beginGroup(type);
        bool ret = m_settings.value(QString::number(index), false).toBool();
        m_settings.endGroup();

        return ret;
    }

private:
    QSettings m_settings;
};

Q_DECL_EXPORT int main(int argc, char **argv)
{
    QGuiApplication::setDesktopFileName(QStringLiteral("com.kdab.toast-policy"));
    QGuiApplication::setApplicationDisplayName(QStringLiteral("Toast Policy"));
    QGuiApplication::setOrganizationName(QStringLiteral("KDAB"));

#ifndef Q_OS_ANDROID
    QApplication app(argc, argv);
#else
    QQuickStyle::setStyle(QStringLiteral("org.kde.breeze"));
    QGuiApplication app(argc, argv);
#endif

    Settings s;
    qmlRegisterSingletonInstance("com.kdab.toast", 1, 0, "Settings", &s);

    QQmlApplicationEngine engine;

    engine.load(QStringLiteral("qrc:/qml/main.qml"));

    return app.exec();
}

#include "main.moc"
