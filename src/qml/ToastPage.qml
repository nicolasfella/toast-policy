import QtQuick 2.15
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.15 as QQC2

import org.kde.kirigami 2.15 as Kirigami
import com.kdab.toast 1.0

Kirigami.ScrollablePage {

    id: root

    title: "Toast Policy"

    required property string type

    ListModel {

        id: kdabModel

        ListElement {
            text: "To world domination"
        }
        ListElement {
            text: "To being awesome"
        }
        ListElement {
            text: "To absent-minded friends"
        }
        ListElement {
            text: "To the mexican donkey in the sky"
        }
        ListElement {
            text: "To drunken logic"
        }
        ListElement {
            text: "To being certifiable"
        }
        ListElement {
            text: "To undefined behavior"
        }
        ListElement {
            text: "To open-mindedness"
        }
        ListElement {
            text: "Further toasts as events (or drinks) may warrant"
        }
    }

    ListModel {

        id: externalModel

        ListElement {
            text: "To whichever event people are meeting for, e.g. \"to successful Developer Days\", \"to a great week of training\", \"to bumping into each other on the 9:14 train\""
        }
        ListElement {
            text: "To world domination"
        }
        ListElement {
            text: "To being awesome"
        }
        ListElement {
            text: "To absent-minded friends"
        }
        ListElement {
            text: "To the mexican donkey in the sky"
        }
        ListElement {
            text: "To drunken logic"
        }
        ListElement {
            text: "To being certifiable"
        }
        ListElement {
            text: "To undefined behavior"
        }
        ListElement {
            text: "To open-mindedness"
        }
        ListElement {
            text: "Further toasts as events (or drinks) may warrant"
        }
    }


    ListView {

        id: lv

        model: root.type === "onlykdab" ? kdabModel : (root.type === "external" ? externalModel : 0)

        delegate: Kirigami.AbstractListItem {
            highlighted: false

            onClicked: {
                cb.checked = !cb.checked
                Settings.setValue(root.type, model.index, cb.checked)
            }

            RowLayout {

                QQC2.CheckBox {
                    id: cb
                    checked: Settings.readValue(root.type, model.index)
                    onToggled: Settings.setValue(root.type, model.index, cb.checked)
                }

                QQC2.Label {
                    text: modelData
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }
            }
        }

        Kirigami.PlaceholderMessage {
            anchors.centerIn: parent
            visible: lv.count === 0
            width: parent.width - (Kirigami.Units.largeSpacing * 8)
            text: "Raising a toast would make you look slightly odd and rather pathetic.\n\nJust keep playing with your BB10 phone."
        }
    }
}
