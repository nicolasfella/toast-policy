import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2

import org.kde.kirigami 2.15 as Kirigami

Kirigami.ApplicationWindow {

    visible: true

    width: 400
    height: 600

    pageStack.initialPage: Qt.resolvedUrl("StartPage.qml")
}
