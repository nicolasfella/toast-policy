import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2

import org.kde.kirigami 2.15 as Kirigami

Kirigami.Page {

    title: "Start"

    Column {
        anchors.fill: parent
        spacing: Kirigami.Units.largeSpacing

        QQC2.Label {

            width: parent.width

            text: "It is of utmost importance to the future success of our business that toasts are being raised correctly. This is why we at KDAB, known as The Best-Organized Company Both Sides Of The River Tay have a Toast Policy so that proper drinking procedure can be followed."

            wrapMode: Text.Wrap
        }

        QQC2.Label {
            text: "Present are:"
        }

        QQC2.Button {
            text: "Two or more KDABians and no non-KDABians"
            width: parent.width
            onClicked: pageStack.push(Qt.resolvedUrl("ToastPage.qml"), {type: "onlykdab"})
        }

        QQC2.Button {
            text: "One or more KDABians and non-KDABians"
            width: parent.width
            onClicked: pageStack.push(Qt.resolvedUrl("ToastPage.qml"), {type: "external"})
        }

        QQC2.Button {
            text: "One KDABian and no non-KDABians"
            width: parent.width
            onClicked: pageStack.push(Qt.resolvedUrl("ToastPage.qml"), {type: "onlyme"})
        }
    }
}
